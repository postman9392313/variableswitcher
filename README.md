# Data source porting to collection variable

## Introduction
This is a sample Postman script that can port the data source to Postman collection variables. This can be useful when the same Postman script needs to run using the Postman collection variables, and at times, using the user input CSV files.

## Getting started
### Prerequisite
1. User has access to Postman application and can import collection files

### Files on this project
| File | Description |
| ------ | ------ |
| Iteration data and collection variable switcher.postman_collection.json | Contains the Postman collection script that can be imported to the Postman application |
| getJoke.csv | Sample CSV file when using the Collection runner |

## Running the project
1. Download the JSON collection and import to Postman
2. Run the collection from Postman

**Notes:**
- Running the collection without specifying an input file will use the available `Postman collection variable`.
- Running the collection with an input file will use the `Postman iteration data`, or the variable values from the input CSV file.